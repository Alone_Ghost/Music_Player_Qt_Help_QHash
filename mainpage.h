#ifndef MAINPAGE_H
#define MAINPAGE_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMediaPlayer>
#include <QString>
#include <QMediaPlayer>
#include <QSlider>
#include <QStatusBar>
#include <QProgressBar>
#include <QMessageBox>
#include <QToolButton>
#include <QMediaPlaylist>
#include <QListWidget>
#include <QFileDialog>

namespace Ui {
class MainPage;
}

class MainPage : public QDialog
{
    Q_OBJECT

public:
    explicit MainPage(QWidget *parent = nullptr);
    ~MainPage();

private:

    Ui::MainPage *ui;
    QPushButton *stop;
    QPushButton *play;
    QPushButton *cancel;
    QPushButton *openFile;
    QPushButton *repeat;
    QHBoxLayout *hboxLayout;
    QHBoxLayout *hboxLayoutTwo;
    QVBoxLayout *vboxlayoutForRepeat;
    QVBoxLayout *vboxLayout;
    QToolButton *buttonrepat;
    QPushButton *resume;
    QPushButton *pause;
    QMediaPlayer *player;
    QMediaPlaylist *playList ;
    QListWidget *listWidget;
    QSlider *slider;
    QProgressBar *bar;

private slots:
    void buttonStop();
    void buttonPlay();
    void openPageOpenFile();
    void buttonCancel();
    void buttonRepeat();

    void doubleClicked(QListWidgetItem *item);
};

#endif // MAINPAGE_H
