# Alone Player

### A Music Player using Qt5

 - List widget using QHash for fine music on QListWidget

###  Dependence:

- `qt5-qtmultimedia-5.12.1-1`

### Generating a `MakeFile`

- `qmake-qt5` 
- `playerGraphic.pro`

### Compile:
You just need run :
`make`

### For run:
`./playerGraphic`
